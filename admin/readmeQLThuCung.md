# QUẢN LÝ THÚ CƯNG
- Đây là bản mô tả các chức năng trang Admin của website PETSHOP

## 🌟 Các METHOD đã viết trong trang ADMIN

### 1. Sử dụng DataTable để hiển thị danh sách thú cưng

![dataTable](imageReadme/DataTable.PNG)

#### 🧱 giải thích các METHOD:

 - METHOD : DataTable
    - Khai báo biến toàn cục :

    ![](imageReadme/varDataTable.PNG)

    - Mục đích chính của khai báo biến toàn cục để dễ dàng sửa đổi và hình dung ra bảng sẽ có những cột như thế nào

    - Khai báo DataTable

    ![](imageReadme/table.PNG)

    - Load Data vào data Table bằng gọi API

    ![](imageReadme/callAPILoadData.PNG)

    - hàm <strong style="color:green">filPetToTable()</strong> có chức năng đưa dữ liệu pet vào bảng
    - hàm <strong style="color:green">callApiLoadData()</strong> được gọi trong <strong style="color:green">onPageLoading()</strong>

    ![](imageReadme/onPageLoading.PNG)


### 2. Các chức năng CRUD

#### ✨ Các Modal:

- MODAL : INSERT PET

![](imageReadme/html_insert_pet.PNG)

* METHOD : gọi API để thêm thú cưng

![](imageReadme/Insert_pet.PNG)

- Hàm <strong style="color:green">callApiInsertPet()</strong> có chức năng gọi API để insert pet lên server

![](imageReadme/call_api_insert.PNG)

<strong>Khi bấm Sửa hoặc xóa, gọi hàm và lưu ID đang sửa hoặc xóa vào biến toàn cục gID</strong>

![](imageReadme/edit_n_dele.PNG)

- MODAL : UPDATE PET

 ![](imageReadme/html_update_pet.PNG)

- METHOD : hiển thị data sẵn có lên modal Update

![](imageReadme/show_update.PNG)

- METHOD : gọi API để UPDATE theo ID

![](imageReadme/call_api_update.PNG)

 - MODAL : DELETE PET

![](imageReadme/delete-modal.PNG)

-METHOD : gọi API để xóa theo ID

![](imageReadme/call_api_delete.PNG)

### 3. FILTER PRICE PET

![](imageReadme/RANGE.PNG)

- METHOD : gọi API theo FILTER

    - lấy giá trị min, max đưa vào biến toàn cục

    ![](imageReadme/min_max.PNG)

    - Gọi API và filter lấy ra những object phù hợp đẩy vào array Toàn cục

    ![](imageReadme/call_api_filter.PNG)

    - Gọi hàm <strong style="color : green">fillPetToTable</strong> theo tham số là ARRAY Toàn Cục vừa tạo 

## 💻 Công nghệ đã dùng
+ Front-End:
    - 1. [Boostrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
    - 2. Javascript
    - 3. Font Awsome
    - 4. Data Table
    - 5. jquery
+ Back-end:
+ KIT:
+ DB: